/*
* Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*/

/* ScriptData
Name: achievement_commandscript
%Complete: 100
Comment: All achievement related commands
Category: commandscripts
EndScriptData */

#include "AchievementMgr.h"
#include "Chat.h"
#include "Language.h"
#include "Player.h"
#include "ScriptMgr.h"

class vip_commandscript : public CommandScript
{
public:
    vip_commandscript() : CommandScript("vip_commandscript") { }

    ChatCommand* GetCommands() const override
    {
        static ChatCommand vipCommandTable[] =
        {
            { "setvip",   SEC_ADMINISTRATOR, false, &HandleSetVipCommand, "", NULL },
            { "setpoint", SEC_ADMINISTRATOR, true, &HandleSetPointCommand, "", NULL },
            { "vip",      SEC_PLAYER, true, &HandleVIPInfoCommand, "", NULL },
            { NULL, 0, false, NULL, "", NULL }
        };
        static ChatCommand commandTable[] =
        {
            { "vip", SEC_ADMINISTRATOR, false, NULL, "", vipCommandTable },
            { "point", SEC_PLAYER, true, &HandlePointCommand, "", NULL },
            { NULL, 0, false, NULL, "", NULL }
        };
        return commandTable;
    }

    static bool HandleVIPInfoCommand(ChatHandler* handler, char const* args)
    {
        handler->PSendSysMessage(LANG_VIP_INFO, handler->GetSession()->GetPlayer()->GetVip());
        return true;
    }

    static bool HandlePointCommand(ChatHandler* handler, char const* args)
    {
        handler->PSendSysMessage(LANG_POINT_INFO, handler->GetSession()->GetPlayer()->GetPoint());
        return true;
    }

    static bool HandleSetVipCommand(ChatHandler* handler, char const* args)
    {
        if (!*args)
            return false;

        char* arg1 = strtok((char*)args, " ");
        char* arg2 = strtok(NULL, " ");
        bool nameStr = true;
        Player *pPlayer = NULL;
        uint32 vip = 0;
        std::string accName;

        if (arg1 && !arg2)
        {
            if (!handler->getSelectedPlayer())
                return false;
            nameStr = false;
        }

        if (!nameStr && !arg1)
            return false;

        if (nameStr)
        {
            std::string name = handler->extractPlayerNameFromLink(arg1);
            pPlayer = ObjectAccessor::FindPlayerByName(args);

            if (pPlayer == NULL)
            {
                handler->SendSysMessage(LANG_PLAYER_NOT_FOUND);
                handler->SetSentErrorMessage(true);
                return false;
            }
        }
        else
        {
            pPlayer = handler->getSelectedPlayer();
            if (pPlayer == NULL)
            {
                handler->SendSysMessage(LANG_NO_CHAR_SELECTED);
                handler->SetSentErrorMessage(true);
                return false;
            }
        }

        vip = (nameStr) ? atoi(arg2) : atoi(arg1);
        AccountMgr::GetName(handler->GetSession()->GetAccountId(), accName);

        if (vip >= 0)
        {
            pPlayer->SetVip(vip);
            handler->PSendSysMessage(LANG_SET_VIP_SUCCESS, pPlayer->GetName(), accName.c_str(), vip);
        }
        else
        {
            handler->SendSysMessage(LANG_SET_VIP_FAILED);
            handler->SetSentErrorMessage(true);
            return false;
        }

        return true;
    }

    static bool HandleSetPointCommand(ChatHandler* handler, char const* args)
    {
        if (!*args)
            return false;

        char* arg1 = strtok((char*)args, " ");
        char* arg2 = strtok(NULL, " ");
        bool nameStr = true;
        Player *pPlayer = NULL;
        int point = 0;
        std::string accName;

        if (arg1 && !arg2)
        {
            if (!handler->getSelectedPlayer())
                return false;
            nameStr = false;
        }

        if (!nameStr && !arg1)
            return false;

        if (nameStr)
        {
            std::string name = handler->extractPlayerNameFromLink(arg1);
            pPlayer = ObjectAccessor::FindPlayerByName(args);
            if (pPlayer == NULL)
            {
                handler->SendSysMessage(LANG_PLAYER_NOT_FOUND);
                handler->SetSentErrorMessage(true);
                return false;
            }
        }
        else
        {
            pPlayer = handler->getSelectedPlayer();
            if (pPlayer == NULL)
            {
                handler->SendSysMessage(LANG_NO_CHAR_SELECTED);
                handler->SetSentErrorMessage(true);
                return false;
            }
        }

        point = (nameStr) ? atoi(arg2) : atoi(arg1);
        sAccountMgr->GetName(handler->GetSession()->GetAccountId(), accName);

        if (point >= 0)
        {
            pPlayer->SetPoint(point);
            handler->PSendSysMessage(LANG_SET_POINT_SUCCESS, pPlayer->GetName(), accName.c_str(), point);
        }
        else
        {
            handler->SendSysMessage(LANG_SET_POINT_FAILED);
            handler->SetSentErrorMessage(true);
            return false;
        }

        return true;
    }
};

void AddSC_vip_commandscript()
{
    new vip_commandscript();
}
