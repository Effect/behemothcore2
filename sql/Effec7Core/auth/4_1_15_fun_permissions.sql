REPLACE INTO fun_permissions VALUES
(1000, 'Command: .anticheat global'),
(1001, 'Command: .anticheat player'),
(1002, 'Command: .anticheat handle'),
(1003, 'Command: .anticheat delete'),
(1004, 'Command: .anticheat jail'),
(1005, 'Command: .anticheat warn'),
(1006, 'Command: .anticheat targetmarker'),
(1007, 'Command: .anticheat');